<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<style type="text/css">
.oh
{
opacity:0.1;
}
.oh:hover
{
opacity:1;
}
</style>
	<div id="sidebar" role="complementary" class="oh">
		<ul>
                        <?php if ( is_single() ) { ?>
                        <li><?php previous_post_link('&larr; %link') ?>
                        <li><?php next_post_link('%link &rarr;') ?>
			<?php } ?>
			<?php 	/* Widgetized sidebar, if you have the plugin installed. */
					if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>

			<li><h2><a href="http://gondwanaland.com/mlog/#me" rel="dc:publisher"><span property="dc:title">Mike Linksvayer</span></a></h2>
<span rel="sioc:has_owner" href="https://creativecommons.net/ml/"></span>
			<em>My opinions only. I do not represent any organization in this publication. <a href="http://gondwanaland.com/mlog/2014/01/01/gnu-pd/#everythingbyme">Everything by me</a>, published anywhere, except where otherwise noted, is <a rel="license" href="http://creativecommons.org/publicdomain/zero/1.0/">dedicated to the public domain</a>.</em></li>
                        <li><h2>Contact</h2>ml@gondwanaland.com / <b><a href="http://identi.ca/mlinksva">pump.io</a></b> / <b><a href="http://en.wikipedia.org/wiki/User:Mike_Linksvayer">enwp</a></b> / <a href="https://joindiaspora.com/u/mlinksva">diaspora</a> / <a href="https://friendica.free-beer.ch/profile/mlinksva">friendica</a> / <a href="http://quitter.se/mlinksva">statusnet</a> / <a href="https://demo.buddycloud.org/mlinksva@buddycloud.org">buddycloud</a> / <a style="color:gray" href="https://mlinksva.cupcake.is">tent</a> / <a style="color:gray" href="http://flickr.com/photos/mlinksva/">Flickr</a> / <a style="color:gray" href="http://twitter.com/mlinksva">Twitter</a> / <a style="color:gray" href="http://www.linkedin.com/in/mlinksva">LinkedIn</a> / <a style="color:gray" href="http://facebook.com/mlinksva">Facebook</a></span>
			</li>

			<li>
				<?php get_search_form(); ?>
			</li>

			<?php endif; ?>


			<?php if ( is_404() || is_category() || is_day() || is_month() ||
						is_year() || is_search() || is_paged() ) {
			?> <li>

			<?php /* If this is a 404 page */ if (is_404()) { ?>

			<?php /* If this is a search result */ } elseif (is_search()) { ?>
			<p>You have searched the <a href="<?php bloginfo('url'); ?>/"><?php bloginfo('name'); ?></a> blog archives
			for <strong>'<?php the_search_query(); ?>'</strong>. If you are unable to find anything in these search results, you can try one of these links.</p>

			<?php } ?>

			</li>
		<?php }?>
		</ul>
		<ul role="navigation">
			<!--?php wp_list_pages('title_li=<h2>Pages</h2>' ); ?-->

			<li><h2><a onclick="document.getElementById('sidebar-archives').style.display='block';" href="javascript:void(0)">Archives</a></h2>
				<ul style="display:none" id="sidebar-archives">
				<?php wp_get_archives('type=monthly'); ?>
				</ul>
			</li>

			<li><h2><a onclick="document.getElementById('sidebar-categories').style.display='block';" href="javascript:void(0)">Categories</a></h2>
				<ul style="display:none" id="sidebar-categories">
			        <?php wp_list_categories('show_count=1&title_li='); ?>
				</ul>
			</li>
		</ul>
		<ul>
			<?php /* If this is the frontpage if ( is_home() || is_page() ) { ?>
				<?php wp_list_bookmarks(); ?>

			<?php } */ ?>

		</ul>

<span style="font-size:25%">153ofsZ1PrnCnDGjvWAenRJc53TRhR9BzK<br/></style>
<a href="http://flattr.com/thing/110223/Mike-Linksvayer-Blog"><img src="http://api.flattr.com/button/flattr-badge-large.png" alt="Flattr this" title="Flattr this" border="0" /></a><br/>
<a href="https://my.fsf.org/associate/support_freedom?referrer=4510"><img src="http://gondwanaland.com/i/fsf-member4510.png" alt="FSF Associate Member" width="88" height="31" /></a>
</div>
